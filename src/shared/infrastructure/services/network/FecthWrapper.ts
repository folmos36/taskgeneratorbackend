import fetch from 'node-fetch';

import { IFecthClientWrapper } from './IFetchClientWrapper';

export class FecthClientWrapper implements IFecthClientWrapper {
  async get(): Promise<string[]> {
    const res = await fetch(
      `${process.env.TITLES_ENDPOINT}=${process.env.NUM_TASKS as string}`,
    );
    const paras = (await res.json()) as string[];
    log.info(
      `response from hipsum`,
      Object.assign({}, { ...metadataLogObject }, { message: paras }),
    );
    return paras;
  }
}

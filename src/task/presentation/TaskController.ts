import { NextFunction, Request, Response } from 'express';
import { NO_CONTENT, OK } from 'http-status';

import { ITaskListQuery } from '../application/queries/getTaskList/ITaskListQuery';
import { TaskListModel } from '../application/queries/getTaskList/TaskListModel';
import { ITaskController } from './ITaskController';

export class TaskController implements ITaskController {
  private _taskListQuery: ITaskListQuery;
  constructor(taskListQuery: ITaskListQuery) {
    this._taskListQuery = taskListQuery;
  }
  getTaskList = async (
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<Response<TaskListModel[]>> => {
    const tasks = await this._taskListQuery.getList();
    return res.status(OK).send([...tasks]);
  };

  markTask = (
    req: Request,
    res: Response,
    next: NextFunction,
  ): Response<void> => {
    log.info(`Task completed: ${JSON.stringify(req.body)}`);
    return res.status(NO_CONTENT).send();
  };
}

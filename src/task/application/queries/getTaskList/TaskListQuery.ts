import { ITaskService } from '../../interfaces/infrastructure/ITaskService';
import { ITaskListQuery } from './ITaskListQuery';
import { TaskListModel } from './TaskListModel';

export class TaskListQuery implements ITaskListQuery {
  _taskService: ITaskService;

  constructor(taskService: ITaskService) {
    this._taskService = taskService;
  }
  async getList(): Promise<TaskListModel[]> {
    const rawTasks = await this._taskService.generate();
    const vmTasks = rawTasks.map(
      (task) => ({ id: task.id, name: task.description } as TaskListModel),
    );
    log.info(
      `response from hipsum`,
      Object.assign(
        {},
        { ...metadataLogObject },
        { message: JSON.stringify(vmTasks) },
      ),
    );
    return vmTasks;
  }
}

import { Router } from 'express';

import {
    ITaskService
} from '../../../../../task/application/interfaces/infrastructure/ITaskService';
import { ITaskListQuery } from '../../../../../task/application/queries/getTaskList/ITaskListQuery';
import { TaskListQuery } from '../../../../../task/application/queries/getTaskList/TaskListQuery';
import { ITaskController } from '../../../../../task/presentation/ITaskController';
import { TaskController } from '../../../../../task/presentation/TaskController';
import { VersionHealth } from '../../../../../version';
import { FecthClientWrapper } from '../../../../infrastructure/services/network/FecthWrapper';
import {
    IFecthClientWrapper
} from '../../../../infrastructure/services/network/IFetchClientWrapper';
import { TaskService } from '../../../services/task/TaskService';

export class Routes {
  public router: Router;
  private versionHealth: VersionHealth;
  private _fetClientWrapper: IFecthClientWrapper = new FecthClientWrapper();
  private _taskService: ITaskService = new TaskService(this._fetClientWrapper);
  private _taskController: ITaskController;
  private _taskListQuery: ITaskListQuery = new TaskListQuery(this._taskService);

  constructor() {
    this.router = Router();
    this.versionHealth = new VersionHealth();
    this._taskController = new TaskController(this._taskListQuery);
  }

  public routes(): Router {
    this.router.get('/version', this.versionHealth.run);
    this.router.get('/tasks', this._taskController.getTaskList);
    this.router.put('/tasks', this._taskController.markTask);
    return this.router;
  }
}

import { createLogger, format, Logger, transports } from 'winston';

import * as packageJSON from '../../../../../package.json';

type metadataLog = {
  time: Date;
  level: number;
  country: string;
  service: string;
  environment: string;
  'app-version': string;
};

declare global {
  type Log = Logger;
  const log: Log;
  const metadataLogObject: metadataLog;
  namespace NodeJS {
    interface Global {
      log: Log;
      metadataLogObject: metadataLog;
    }
  }
}

export class WinstonLogger {
  private readonly log: Logger;
  private readonly metadata: metadataLog;

  constructor() {
    const logLevel = process.env.NODE_ENV !== 'production' ? 'debug' : 'info';
    const { json } = format;
    global.metadataLogObject = this.metadata = {
      time: new Date(),
      level: 30,
      country: process.env.COUNTRY as string,
      service: `${process.env.SERVICE}-${process.env.ENVIRONMENT_TYPE}`,
      environment: `${process.env.NODE_ENV}`,
      'app-version': `${packageJSON.version}`,
    };

    this.log = createLogger({
      transports: [
        new transports.Console({
          format: json(),
          level: logLevel,
        }),
      ],
    });
  }

  // eslint-disable-next-line no-return-assign
  initializer = (): Logger => (global.log = this.log);
}

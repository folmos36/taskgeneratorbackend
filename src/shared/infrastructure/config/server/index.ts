import bodyParser from 'body-parser';
import cors from 'cors';
import dotenv from 'dotenv';
import express, { Application } from 'express';
import helmet from 'helmet';

import { WinstonLogger } from '../logger';
import { Routes } from './routes';

dotenv.config();

class App {
  public server: Application;
  public log: WinstonLogger = new WinstonLogger();
  public appRoutes: Routes = new Routes();
  private BASE_PATH: string = process.env.BASE_PATH || '/api';

  constructor() {
    this.server = express();
    this.log.initializer();
    this.config();
  }

  private config(): void {
    this.server.use(cors());
    this.server.use(helmet());
    this.server.use(express.json());
    this.server.use(express.urlencoded({ extended: false }));
    this.server.use(this.BASE_PATH, this.appRoutes.routes());
    this.log.initializer();
  }
}

export default new App().server;

import { Task } from '../../../domain/Task';

export interface ITaskService {
  generate(): Promise<Task[]>;
}

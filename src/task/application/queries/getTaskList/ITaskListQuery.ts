import { TaskListModel } from './TaskListModel';

export interface ITaskListQuery {
  getList(): Promise<TaskListModel[]>;
}

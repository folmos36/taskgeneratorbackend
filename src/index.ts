import express from './shared/infrastructure/config/server';

const PORT: string = (process.env.PORT_SERVER as string) || '5000';

const server = express.listen(PORT, () => {
  log.info(`Server listening on port ${PORT}!`, metadataLogObject);
});

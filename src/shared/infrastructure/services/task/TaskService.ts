import { Task } from 'task/domain/Task';
import { v4 as uuid } from 'uuid';

import { ITaskService } from '../../../../task/application/interfaces/infrastructure/ITaskService';
import { IFecthClientWrapper } from '../network/IFetchClientWrapper';

export class TaskService implements ITaskService {
  private _fecthWrapper: IFecthClientWrapper;

  constructor(fetchWrapper: IFecthClientWrapper) {
    this._fecthWrapper = fetchWrapper;
  }

  async generate(): Promise<Task[]> {
    const paras = await this._fecthWrapper.get();
    const tasks = paras.map(
      (para) => ({ id: uuid(), description: para } as Task),
    );
    log.info(
      `tasks generated`,
      Object.assign(
        {},
        { ...metadataLogObject },
        { message: JSON.stringify(tasks) },
      ),
    );
    return tasks;
  }
}

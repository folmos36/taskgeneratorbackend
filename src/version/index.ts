import { Request, Response } from 'express';
import httpStatus from 'http-status';

import * as packageJSON from '../../package.json';

export class VersionHealth {
  run = async (req: Request, res: Response): Promise<void> => {
    try {
      const version = {
        app: packageJSON.name,
        version: packageJSON.version,
        env: process.env.ENVIRONMENT_TYPE,
      };
      res.status(httpStatus.OK).send({ ...version });
    } catch (error) {
      res.status(httpStatus.BAD_REQUEST).json(error);
    }
  };
}

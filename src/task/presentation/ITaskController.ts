import { NextFunction, Request, Response } from 'express';

import { TaskListModel } from '../application/queries/getTaskList/TaskListModel';

export interface ITaskController {
  getTaskList(
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<Response<TaskListModel[]>>;

  markTask(req: Request, res: Response, next: NextFunction): Response<void>;
}
